/* Copyright 1997 Acorn Computers Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* ExtrasLib:SubFlex.h */

/* A flex block within a flex block
 *
 * Authors:
 *      Peter Hartley       <peter@ant.co.uk>
 */


#ifndef ExtrasLib_SubFlex_h
#define ExtrasLib_SubFlex_h

#ifndef ExtrasLib_MemFlex_h
#include "memflex.h"
#endif

typedef int* subflex_ptr;

os_error *SubFlex_Initialise( flex_ptr master );

os_error *SubFlex_Alloc( subflex_ptr anchor, int size, flex_ptr master );

os_error *SubFlex_MidExtend( subflex_ptr anchor, int at, int by,
                             flex_ptr master );

os_error *SubFlex_Free( subflex_ptr anchor, flex_ptr master );

int       SubFlex_Size( subflex_ptr anchor, flex_ptr master );

int SubFlex_Reanchor( subflex_ptr anchor, flex_ptr master );

#endif
